import setuptools

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

setuptools.setup(
    name="mtcnn_mxnet",
    version="0.1",
    description="MTCNN implemented in MXNet",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/techvico/mtcnn_mxnet_lib",
    packages=setuptools.find_packages(),
    package_data={"": ["models/*"]},
    install_requires=[
        "mxnet-cu102",
        "numpy"
    ],
    python_requires=">=3.6",
)
